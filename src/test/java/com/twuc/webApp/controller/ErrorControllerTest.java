package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ErrorControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    void should_return_status_code_500_runtime_exception() throws Exception {
        ResponseEntity<String> entity = template
                .getForEntity("/api/errors/default", String.class);

        assertEquals(500, entity.getStatusCodeValue());
    }

    @Test
    void should_return_status_code_500_illegalargument_exception() throws Exception {
        ResponseEntity<String> entity = template
                .getForEntity("/api/errors/illegal-argument", String.class);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_return_status_code_418_null_point_exception() throws Exception {
        ResponseEntity<String> entity = template
                .getForEntity("/api/errors/null-pointer", String.class);

        assertEquals(418, entity.getStatusCodeValue());
    }

    @Test
    void should_return_status_code_418_arithmetic_exception() throws Exception {
        ResponseEntity<String> entity = template
                .getForEntity("/api/errors/arithmetic", String.class);

        assertEquals(418, entity.getStatusCodeValue());
    }

    @Test
    void should_return_status_code_418_using_controller_advicer() throws Exception {
        ResponseEntity<String> entity1 = template
                .getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(418, entity1.getStatusCodeValue());
        assertEquals(MediaType.APPLICATION_JSON, entity1.getHeaders().getContentType());
        assertEquals("{\"message\": \"Something wrong with sister.\"}", entity1.getBody());

        ResponseEntity<String> entity2 = template
                .getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(418, entity2.getStatusCodeValue());
        assertEquals(MediaType.APPLICATION_JSON, entity2.getHeaders().getContentType());
        assertEquals("{\"message\": \"Something wrong with brother.\"}", entity2.getBody());
    }
}

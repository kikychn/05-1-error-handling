package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/errors")
public class ErrorController {

    @GetMapping("/default")
    public ResponseEntity<String> throwRuntimeException() {
        throw new RuntimeException("runtime exception");
    }

    @GetMapping("/illegal-argument")
    public ResponseEntity<String> throwIllegalArgumentException() {
        throw new IllegalArgumentException("Something wrong with the argument");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception exception) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(exception.getMessage());
    }

    @GetMapping("/null-pointer")
    public ResponseEntity<String> throwNullPointerException() {
        throw new NullPointerException();
    }

    @GetMapping("/arithmetic")
    public ResponseEntity<String> throwArithmeticException() {
        throw new ArithmeticException();
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> handleTwoClassesException(Exception exception) {
        return ResponseEntity
                .status(418)
                .body(exception.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(Exception exception) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(exception.getMessage());

    }
}

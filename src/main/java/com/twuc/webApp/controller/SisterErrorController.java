package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SisterErrorController {

    @GetMapping("/sister-errors/illegal-argument")
    public ResponseEntity<String> throwIllegalArgumentException() {
        throw new IllegalArgumentException("Something wrong with sister.");
    }
}
